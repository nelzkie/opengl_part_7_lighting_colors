
/**********************************

One thing left to note is that when we start to change the vertex and fragment shaders, the lamp cube will change as well and this is not what we want. 
We don't want the lamp object's color to be affected by the lighting calculations in the upcoming tutorials, but rather keep the lamp isolated from the rest. 
We want the lamp to have a constant bright color, unaffected by other color changes (this makes it look like the lamp really is the source of the light).

To accomplish this we actually need to create a second set of shaders that we will use to draw the lamp, thus being safe from any changes to the lighting shaders. 

***************************************/


#version 330 core
out vec4 color;

void main()
{
    color = vec4(1.0f); // Set alle 4 vector values to 1.0f
}