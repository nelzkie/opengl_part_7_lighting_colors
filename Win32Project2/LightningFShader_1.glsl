#version 330 core
out vec4 color;
  
uniform vec3 objectColor;
uniform vec3 lightColor;

void main()
{
	/**********************
	If we would then multiply the light source's color with an object's color value, the resulting color is the reflected color of the object (and thus its perceived color)

	*************************/
    color = vec4(lightColor * objectColor, 1.0f);
}